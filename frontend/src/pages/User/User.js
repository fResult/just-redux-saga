import React, { useCallback, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { UserTemplate } from '../../components/templates'
import { fetchUsersRequest } from "../../redux/actions/user/user.action";

const columns = [
  { title: 'Username', dataIndex: 'username', key: 'username' },
  { title: 'Name', dataIndex: 'name', key: 'name' },
  { title: 'Email', dataIndex: 'email', key: 'email' },
  { title: 'Phone', dataIndex: 'phone', key: 'phone' },
  { title: 'Website', dataIndex: 'website', key: 'website' }
]

const User = ({ ...props }) => {
  const { loading, users = [], error } = useSelector((state) => state.user)
  const dispatch = useDispatch()

  const fetchUsers = useCallback(() => dispatch(fetchUsersRequest()), [
    dispatch
  ])

  const dataSource = users?.map((user, idx) => {
    return { key: user.id, ...user }
  })

  useEffect(() => {
    fetchUsers()
  }, [fetchUsers])

  return (
    <UserTemplate {...{ columns, dataSource, loading, error, fetchUsers }} />
  )
}

export default User
