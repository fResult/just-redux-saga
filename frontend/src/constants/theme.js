const theme = {
  colors: {
    black: '#000000',
    white: '#ffffff',
    blue: '#0000ff',
    navy: '#000080'
  }
}

export default theme
