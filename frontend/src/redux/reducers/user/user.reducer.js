import {
  USER_FETCH_BY_ID_REQUEST,
  USER_FETCH_BY_ID_SUCCESS,
  USERS_FETCH_FAILURE,
  USERS_FETCH_REQUEST,
  USERS_FETCH_SUCCESS
} from '../../actions/user/user.action'

const initialState = {
  loading: false,
  users: [],
  error: ''
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USERS_FETCH_REQUEST:
      return { ...state, loading: true }
    case USERS_FETCH_SUCCESS:
      return {
        loading: false,
        users: action.payload,
        error: ''
      }
    case USERS_FETCH_FAILURE:
      return {
        loading: false,
        users: [],
        error: action.payload
      }
    case USER_FETCH_BY_ID_REQUEST:
      return { ...state, loading: false }
    case USER_FETCH_BY_ID_SUCCESS:
      return {
        loading: false,
        user: action.payload,
        error: ''
      }
    default:
      return state
  }
}
export default userReducer
