export const USERS_FETCH_FAILURE = 'USERS_FETCH_FAILURE'

export const USER_FETCH_BY_ID_REQUEST = 'USERS_FETCH_BY_ID_REQUEST'
export const USER_FETCH_BY_ID_SUCCESS = 'USER_FETCH_BY_ID_SUCCESS'

export const USERS_FETCH_REQUEST = 'USERS_FETCH_REQUEST'
export const USERS_FETCH_SUCCESS = 'USERS_FETCH_SUCCESS'

export const fetchUserByIdRequest = (id) => {
  return {
    type: USER_FETCH_BY_ID_REQUEST,
    payload: id
  }
}

export const fetchUserByIdSuccess = (user) => {
  return {
    type: USER_FETCH_BY_ID_SUCCESS,
    payload: user
  }
}
export const fetchUsersRequest = () => {
  return {
    type: USERS_FETCH_REQUEST
  }
}

export const fetchUsersSuccess = (users) => {
  return {
    type: USERS_FETCH_SUCCESS,
    payload: users
  }
}

export const fetchUsersFailure = (error) => {
  return {
    type: USERS_FETCH_FAILURE,
    payload: error
  }
}
