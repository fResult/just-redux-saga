import { call, put, takeLatest } from 'redux-saga/effects'
import * as API from '../../../services/api'
import * as Action from '../../actions/user/user.action'

function* fetchUsers(action) {
  try {
    const users = yield call(API.fetchUsers, action.payload) || []
    yield put(Action.fetchUsersSuccess(users))
  } catch (err) {
    yield put(Action.fetchUsersFailure(`ERROR!!: ${err.message}`))
  }
}

function* fetchUserById(action) {
  try {
    const user = yield call(API.fetchUserById, action.payload) || {}
    yield put(Action.fetchUserByIdSuccess(user))
  } catch (err) {
    yield put(Action.fetchUsersFailure(`ERROR!!: ${err.message}`))
  }
}

function* userSaga() {
  yield takeLatest(Action.USERS_FETCH_REQUEST, fetchUsers)
  yield takeLatest(Action.USER_FETCH_BY_ID_REQUEST, fetchUserById)
}

export default userSaga
