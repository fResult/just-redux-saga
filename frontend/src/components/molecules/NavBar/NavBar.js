import React from 'react'
import styled from 'styled-components'
import { Menu } from '../../atoms'

const NavBar = () => {
  return (
    <Menu mode="horizontal">
      <Menu.Item>Menu 1</Menu.Item>
      <Menu.Item>Menu 2</Menu.Item>
    </Menu>
  )
}

export default styled(NavBar)``

