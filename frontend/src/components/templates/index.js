import User from './User'
import Layout from './PageLayout'

export { User as UserTemplate, Layout }
