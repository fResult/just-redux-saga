import React from 'react'
import { UserTable, UserTitle } from '../../organisms'

const User = ({ ...props }) => {
  return (
    <div>
      <UserTitle />
      <UserTable {...props} />
    </div>
  )
}

export default User
