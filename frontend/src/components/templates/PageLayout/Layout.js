import React from 'react'
import { Layout, Content, Footer, Header } from '../../organisms'

export default ({ children, ...props }) => {
  return (
    <Layout>
      <Header />
      <Content>{children}</Content>
      <Footer />
    </Layout>
  )
}
