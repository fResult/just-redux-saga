import styled from 'styled-components'
import { layout, color, space } from 'styled-system'
import Layout from "../Layout/Layout";

const { Content: ContentAntD } = Layout

const Content = styled(ContentAntD)`
  ${layout}
  ${color}
  ${space}
`

export default Content
