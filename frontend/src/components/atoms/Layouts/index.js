import Layout from './Layout'
import Content from './Content'
import Footer from './Footer'
import Header from './Header'

export { Layout, Header, Content, Footer }
