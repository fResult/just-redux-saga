import { Layout as LayoutAntD  } from 'antd'
import styled from 'styled-components'
import { layout, space, color } from 'styled-system'


const Layout = styled(LayoutAntD)`
  ${layout}
  ${space}
  ${color}
`

export default Layout
