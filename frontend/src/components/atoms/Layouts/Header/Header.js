import styled from 'styled-components'
import { layout, color, space } from 'styled-system'
import Layout from '../Layout/Layout'

const { Header: HeaderAntD } = Layout

const Header = styled(HeaderAntD)`
  ${layout}
  ${color}
  ${space}
`

export default Header
