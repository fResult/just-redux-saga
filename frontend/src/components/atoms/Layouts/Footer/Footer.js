import styled from 'styled-components'
import { layout, color, space } from 'styled-system'
import Layout from "../Layout/Layout";

const { Footer: FooterAntD } = Layout

const Footer = styled(FooterAntD)`
  ${layout}
  ${color}
  ${space}
`

export default Footer
