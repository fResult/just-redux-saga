import { Typography } from 'antd'
import { typography, color, space } from 'styled-system'
import styled from 'styled-components'

const { Text: TextAntD } = Typography

const Text = styled(TextAntD)`
  ${typography}
  ${color}
  ${space}
`

export default Text
