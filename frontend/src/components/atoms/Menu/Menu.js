import styled from 'styled-components'
import { Menu as MenuAntD } from 'antd'

const Menu = styled(MenuAntD)`
  background: transparent;
  color: #f0ffff;
  height: 100%;
`

export default Menu
