import styled from 'styled-components'
import { Button as ButtonAntD } from 'antd'

const Button = styled(ButtonAntD)`
  border-radius: 5px;
`

export default Button
