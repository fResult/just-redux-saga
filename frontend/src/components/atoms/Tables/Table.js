import React from 'react'
import { Table as TableAntD } from 'antd'
import styled from 'styled-components'

const Table = styled(TableAntD)`
  /* Custom your styled table */
`

export default ({ columns = [], dataSource = [], loading, ...props }) => {
  return <Table {...{ columns, dataSource, loading, props }} />
}
