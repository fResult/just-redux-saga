import { Typography } from 'antd'
import { color, typography } from 'styled-system'
import styled from 'styled-components'

const { Title: TitleAntD } = Typography

const Title = styled(TitleAntD)`
  ${typography}
  ${color}
  
  &.ant-typography {
    color: ${({ color }) => color};
  }
`

export default Title
