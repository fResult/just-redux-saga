import Menu from './Menu'
import Title from './Title'
import Text from './Text'
import Button from './Button'
import Table from './Tables'
import { Layout, Header, Content, Footer } from './Layouts'

export { Title, Text, Button, Menu, Table, Layout, Header, Content, Footer }
