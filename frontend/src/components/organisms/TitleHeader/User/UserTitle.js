import React from 'react'
import { Text, Title } from '../../../molecules'
import { FiUsers } from 'react-icons/fi'

const UserTitle = (props) => {
  return (
    <div>
      <Title color="#3182d0" style={{ fontSize: 48 }}>
        <FiUsers /> User Page
      </Title>
      <Text color="#3182d0" mb="20px" style={{ display: 'inline-block' }}>
        <span style={{ color: 'seagreen', fontWeight: '700px' }}>
          Description:
        </span>{' '}
        just a fetching user demonstrate
      </Text>
    </div>
  )
}

export default UserTitle
