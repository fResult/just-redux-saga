import React from 'react'
import { Table, Button } from '../../../molecules'

const UserTable = (props) => {
  return (
    <div>
      <Button
        {...{
          type: 'primary',
          style: { height: 40 },
          loading: props.loading,
          onClick() {
            props.fetchUsers()
          }
        }}
      >
        Fetch Users
      </Button>
      {props.loading ? (
        <div>LOADING...</div>
      ) : props.error ? (
        <h1>FETCH ERROR</h1>
      ) : (
        <Table {...props} />
      )}
    </div>
  )
}

export default UserTable
