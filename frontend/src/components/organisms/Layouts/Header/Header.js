import React from 'react'
import { Header, NavBar } from '../../../molecules'

export default () => {
  return (
    <Header>
      <NavBar />
    </Header>
  )
}

