import { Layout, Header, Content, Footer } from './Layouts'
import { UserTable } from './Table'
import { UserTitle } from './TitleHeader'

export { Layout, Header, Content, Footer, UserTable, UserTitle }
