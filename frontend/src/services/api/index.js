import axios from 'axios'

export const fetchUsers = async () => {
  const { data: users } = await axios.get(
    'https://jsonplaceholder.typicode.com/users'
  )
  return users
}

export const fetchUserById = async (id) => {
  const { data: user } = await axios.get(
    `https://jsonplaceholder.typicode.com/users/${id}`
  )
  return user
}
