import { Provider } from 'react-redux'
import React from 'react'
import 'antd/dist/antd.css'
import ReactDOM from 'react-dom'
import theme from './constants/theme'
import store from './redux/store'
import * as serviceWorker from './serviceWorker'
import './index.css'

import App from './App'
import { ThemeProvider } from 'styled-components'

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider {...{ theme }}>
      <App />
    </ThemeProvider>
  </Provider>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
